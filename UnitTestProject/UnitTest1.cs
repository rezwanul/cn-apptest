﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AppTest;

namespace UnitTestProject
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void Check_Operations()
        {
            string filePath = @"C:\MyFile.txt";
            double expected = 90;
            
            Assert.AreEqual(expected, Program.GetValues(filePath));
        }
    }
}
