﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppTest
{
    public class AllOperations
    {
        protected static double BasicOperations(double value1, string operatorString, double value2)
        {
            double val = 0;
            switch (operatorString)
            {
                case "add":
                    val = value1 + value2;
                    break;
                case "subtract":
                    val = value1 - value2;
                    break;
                case "multiply":
                    val = value1 * value2;
                    break;
                case "divide":
                    val = value1 / value2;
                    break;
                default:
                    val = value1;
                    break;

            }
            return val;
        }
    }
}
