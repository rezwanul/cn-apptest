﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace AppTest
{
    public class Program : AllOperations
    {
        static void Main(string[] args)
        {
            double i = GetValues(@"C:\MyFile.txt");

            Console.WriteLine(i);
            Console.ReadKey();
        }

        public static double GetValues(string filePath)
        {
            try
            {
                double result = 0;
                string text = System.IO.File.ReadAllText(filePath);

                Dictionary<string, string> dict = text.Split(new[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries)
                   .Select(part => part.Split(' '))
                   .ToDictionary(split => split[0], split => split[1]);


                result = Convert.ToDouble(dict.Values.ElementAt(dict.Count - 1));                

                foreach (KeyValuePair<string, string> entry in dict)
                {
                    result = BasicOperations(result, entry.Key, Convert.ToDouble(entry.Value));
                }

                return result;
            }
            catch (FormatException e)
            {
                throw e;
            }
            catch (ArgumentOutOfRangeException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                throw e;
            }
        }      
    }
}
